def isPerfectSquare(num):
    left = 1
    right = num

    while left <= right:
        mid = left + (right - left) // 2
        square = mid * mid
        if square < num:
            left = mid + 1
        elif square > num:
            right = mid - 1
        else:
            return True
    return False
x=int(input())
print(isPerfectSquare(x))