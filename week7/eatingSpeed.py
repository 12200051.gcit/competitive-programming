import math

def minEatingSpeed(List,h):
        l, r = 1, max(List)
        res = r

        while l <= r:
            k = (l + r) // 2
            hours = 0
            for p in List:
                hours += math.ceil(p / k)

            if hours <= h:
                res = min(res, k)
                r = k - 1
            else:
                l = k + 1
        return res

n = [30,11,23,4,20]
h=5
print(minEatingSpeed(n,h))