#factorial
def fact(num):
    if num==1:
        return num;
    else:
        return (num* fact(num-1));

#fibonacci
def febo(n):
	if n == 0:
		return 0
	
	if n == 1:
		return 1

	f1 = febo(n-1)
	f2 = febo(n-2)

	return f1 + f2

#Power
def power(a, n):
	if n == 0:
		return 1
	return a * power(a, n - 1)

def fastPower(a, n):
	if n == 0:
		return 1
	subProb = fastPower(a, n//2)
	subProbSquare = subProb * subProb
	if n & 1:
		return a * subProbSquare
	return subProb


print(fact(5)); 

print(febo(5));

print(power(2,5))

print(fastPower(2,5))