def arraySortedOrNot(arr, n):
	if (n == 0 or n == 1):
		return True

	return (arr[n - 1] >= arr[n - 2] and
			arraySortedOrNot(arr, n - 1))

arr = [ 1,2,3,4,5,6 ]
n = len(arr)

if (arraySortedOrNot(arr, n)):
	print("Yes")
else:
	print("No")