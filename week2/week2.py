def Get(x, i):
    mask = 1 << i;
    answer = x | mask;
    
    if(answer>x):
        print(0);
    else :
        print(1);

def Clear(x, i):
    mask = 1 << i;
    answer = x ^ mask;
    
    if answer:
        print(answer);
    else:
        print(0);

def Set(x, i):
    mask = 1 << i;
    answer = x | mask;
    
    if answer:
        print(answer);
    else:
        print(0);
        
def Update(x,i,upNum):
    mask = 1<<i;
    if(upNum==1):
        answer = x | mask;
        print(answer);
    else:
        answer = x ^ mask;
        print(answer);

def  countSetBits(n):
    count = 0
    while (n):
        count += n & 1
        n >>= 1
    return count

def DecimalToBinary(x):
     
    if x > 1:
        DecimalToBinary(x // 2)
    print(x % 2, end = '')
    
def  countSetBits(n):
    count = 0
    while (n):
        count += n & 1
        n >>= 1
    if(count==1):
        print("It can be represented in power of 2");
    else:
        print("It can not be represented in power of 2");
def length(x):
    count = 0;
    while(x):
        count += 1;
        x >>= 1;
        
    return count;

if __name__ == "__main__" :
    Get(10,1);
    
    Clear(10,1);
    
    Set(10,0);
    
    upNum = 0;
    Update(10,1,upNum);
    
    n = 11;
    print(countSetBits(n));
    
    x = 10
    DecimalToBinary(x)
    
    n = 8;
    countSetBits(n);
    
    x = 10;
    print(length(x));