#SPOJ
def gcd(a, b):
    if (a == 0):
        return b
    if (b == 0):
        return a
  
    if (a == b):
        return a
  
    if (a > b):
        return gcd(a-b, b)
    return gcd(a, b-a)
  
n=2
x=int(input(""))
y=int(input(""))
for i in range(1,n-1):
    x=int(input(x))
    y=int(input(y))
print(gcd(x,y))